pandas==1.1.0
gplearn==0.4.1
sympy==1.7.1
pygmo==2.16.1
matplotlib>=3.3.0
