.. _installation:

Installation
============

``pyPTF`` requires a recent version of gplearn (which requires scikit-learn, numpy and
scipy). So first you will need to `follow their installation instructions <https://gplearn.readthedocs.io/en/stable/installation.html>`_
to get the dependencies.

You can install ``pyPTF`` using pip::

    pip install pyPTF

Or if you wish to install to the home directory::

    pip install --user pyPTF

For the latest development version, first get the source from gitlab::

    git clone https://gitlab.com/spadarian/pyPTF.git

Then navigate into the local ``pyPTF`` directory and simply run::

    python setup.py install

or::

    python setup.py install --user

and you're done!
