.. _reference:

API reference
=============

.. automodule:: pyPTF

.. autosummary::
    :toctree: _autosummary

    ptf
    base_fkmeans
    fkmeans
    optim
    utils

