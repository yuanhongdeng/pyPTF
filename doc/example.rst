.. _example:

Basic example
-------------

In this example I will walk you through the whole process of generating
a pedotransfer function (PTF), including:

* training to model
* assessing the uncertainty
* preparing the PTF for sharing and publishing


Load dataset
~~~~~~~~~~~~

Let's start by loading a dataset included with ``pyPTF``. The ``water``
dataset includes 934 soil samples with information about bulk density,
clay and sand content, and water holding capacity at -33 and -1500 kPa.

.. code:: python3

    from pyPTF.datasets import load_water
    import pandas as pd

    data = load_water()
    data.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>clay</th>
          <th>sand</th>
          <th>bd</th>
          <th>h333</th>
          <th>h15000</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>45.0</td>
          <td>38.0</td>
          <td>1.25</td>
          <td>0.406</td>
          <td>0.26</td>
        </tr>
        <tr>
          <th>1</th>
          <td>52.0</td>
          <td>33.0</td>
          <td>1.33</td>
          <td>0.446</td>
          <td>0.32</td>
        </tr>
        <tr>
          <th>2</th>
          <td>49.0</td>
          <td>36.0</td>
          <td>1.32</td>
          <td>0.378</td>
          <td>0.28</td>
        </tr>
        <tr>
          <th>3</th>
          <td>48.0</td>
          <td>27.0</td>
          <td>1.40</td>
          <td>0.369</td>
          <td>0.35</td>
        </tr>
        <tr>
          <th>4</th>
          <td>49.0</td>
          <td>31.0</td>
          <td>1.25</td>
          <td>0.371</td>
          <td>0.23</td>
        </tr>
      </tbody>
    </table>
    </div>



Creating a PTF
~~~~~~~~~~~~~~

Now we will use the ``PTF`` class to create a pedotransfer function to
predict water content at -1500 kPa using clay and sand as predictors.

We specify a formula with the format :math:`y \sim x_0+x_1+...x_n` to
run the training for 20 generations.

.. code:: python3

    from pyPTF.ptf import PTF

    formula = 'h15000~clay+sand'
    model1500 = PTF(data,
                    formula,
                    {'generations': 20, 'metric': 'rmse'},
                    simplify=True).fit()


.. parsed-literal::
    \
        |    Population Average   |             Best Individual              |
    ---- ------------------------- ------------------------------------------ ----------
     Gen   Length          Fitness   Length          Fitness      OOB Fitness  Time Left
       0    38.43 4.4494788656963616e+17       31 0.07408611850111413 0.06740493008696294      1.38m
       1    26.71 5531523.539243222       27 0.05265940374353026 0.050124923985138826      2.16m
       2    19.49 91070.84177217142       23 0.04258389954469868 0.05360068967331263      2.28m
       3    19.51 3322022.8409649567       23 0.042564997217998474 0.053735161896910924      2.28m
       4    24.52 11496.767683390117       27 0.042544010226173705 0.053884001741728804      2.30m
       5    21.46 7784.547680440191       21 0.04185020963839345 0.05803052751363165      2.22m
       6    21.22 1498.423606571256       21 0.0416623841298181 0.06137130750913531      2.08m
       7    18.28 18379.272069603296       25 0.04164108732522575 0.059876101201845056      1.93m
       8    16.49 3703.6625628961815       23 0.04158579838144309 0.06021970440177257      1.77m
       9    14.91 81848.4604886528       17 0.041655476432138544 0.0598932065348786      1.60m
      10    14.03 692623.0544381787       13 0.041101169612849736 0.06219898078906755      1.44m
      11     13.4 22455698.322825372       13 0.04190643149446103 0.05717765051917337      1.29m
      12    13.16 837.9906552081756       13 0.04108296772857316 0.06230672605406894      1.13m
      13    12.77 1390279.764570951       13 0.04139649425985193 0.060417106786183714     58.37s
      14    12.68 19785324.389812794       13 0.041544891068983604 0.0594966360390584     48.78s
      15    12.62 6376295.423395667       13 0.04118946496617664 0.06167296545930717     38.67s
      16    12.43 5183.622562700015       13 0.0412845144578868 0.06110037278645028     28.89s
      17    12.32 7774019452.996211       11 0.04158291135771948 0.05953314070673189     19.23s
      18    12.38 355765.60841212823       11 0.04149765274330074 0.06006426851126114      9.57s
      19    12.31 2835.372441882708       13 0.040839295228477485 0.06372710563967807      0.00s


The final solution is stored in the ``.ptf`` attribute:

.. code:: python3

    print(model1500.ptf)


.. parsed-literal::
    \
    0.738*clay/(2*clay + sand + 0.322)


Or a bit prettier in LaTeX format:

.. code:: python3

    model1500.to_latex()




.. parsed-literal::
    \
    '\\frac{0.738 clay}{2 clay + sand + 0.322}'



:math:`\frac{0.738 clay}{2 clay + sand + 0.322}`

The validation statistics can be obtained by accessing the ``.stats``
attribute:

.. code:: python3

    print(model1500.stats)


.. parsed-literal::
    \
    {'R2': 0.771299228437424, 'RMSE': 0.043680111721930495}


Making predictions with the new PTF is simple. We just need to call the
``.predict()`` method on some new data containing the predictor
variables (clay and sand):

.. code:: python3

    # Select the first 5 rows of the original data
    new_data = data.iloc[:5, :]
    model1500.predict(new_data)




.. parsed-literal::
    \
    array([0.258557  , 0.27919531, 0.2689638 , 0.28697602, 0.2793628 ])



Uncertainty assessment
~~~~~~~~~~~~~~~~~~~~~~

So far we have created a PTF and we are able to make predictions with
it. Great! Now it is time to assess the uncertainty of the PTF, because
we know the model is not perfect.

In order to do that, we can use the ``FKMEx`` class to use the fuzzy
k-means with extragrades algorithm to define combinations of our dataset
with different error magnitudes.

The idea of the following code is to run the clustering algorithm using
different number of classes (2 to 12 in this case) to find the optimum
number of clusters. Depending on your computer, this might take a while.
In my case, around 20 minutes.

.. code:: python3

    from pyPTF.fkmeans import FKMEx

    all_k = []
    for n_k in range(2, 12 + 1):
        k = FKMEx(n_k, phi=1.5, disttype='mahalanobis')
        k.fit(model1500)
        all_k.append(k)

.. code:: python3

    all_k




.. parsed-literal::
    \
    [<FKMEx (fitted) classes=2 phi=1.5 dist=mahalanobis>,
     <FKMEx (fitted) classes=3 phi=1.5 dist=mahalanobis>,
     <FKMEx (fitted) classes=4 phi=1.5 dist=mahalanobis>,
     <FKMEx (fitted) classes=5 phi=1.5 dist=mahalanobis>,
     <FKMEx (fitted) classes=6 phi=1.5 dist=mahalanobis>,
     <FKMEx (fitted) classes=7 phi=1.5 dist=mahalanobis>,
     <FKMEx (fitted) classes=8 phi=1.5 dist=mahalanobis>,
     <FKMEx (fitted) classes=9 phi=1.5 dist=mahalanobis>,
     <FKMEx (fitted) classes=10 phi=1.5 dist=mahalanobis>,
     <FKMEx (fitted) classes=11 phi=1.5 dist=mahalanobis>,
     <FKMEx (fitted) classes=12 phi=1.5 dist=mahalanobis>]



In order to estimate the uncertainty, it is necessary that we know how
the PTF performs with different combinations of the predictors. We can
do that with the data we used to train it the PTF:

.. code:: python3

    # Predicted values
    pred = model1500.predict(model1500.cleaned_data)
    # Observed values
    obs = model1500.cleaned_data[model1500.y].values

Now that we know the error we can estimate the prediction interval of
each observation. With those intervals we can calculate the prediction
interval coverage probability (probability of an observation falling
within the prediction interval)

:math:`PICP = \frac{1}{n}\,\text{count(}a\text{)} \qquad a:PL_i^L\leq p_i\leq PL_i^U`

and the mean prediction interval

:math:`MPI = \frac{1}{n}\sum_{i=1}^n\,[PL_i^U - PL_i^L]`.

By plotting both, :math:`PICP` and :math:`MPI`, we can obtain the
optimal numebr of clusters.

.. code:: python3

    from pyPTF.utils import find_optim_cluster

    find_optim_cluster(all_k, obs, pred)



.. image:: _static/example_files/example_19_0.png


To select the opmial number of clusters, we need to find :math:`PICP`
closer to our confidence value (95%), and with the lowest :math:`MPI`.
In this case 6 and 9 are close to 95% :math:`PICP` but 9 has a lower
:math:`MPI`.

Now that we have the optimal number of clusters, let's explore the
``FKMEx`` instance.

.. code:: python3

    k9 = all_k[7]

The first expected output of a clustering algorithm is the centroids of
each class:

.. code:: python3

    k9.centroids




.. parsed-literal::
    \
    array([[28.33741154, 59.08412785],
           [16.88978997, 71.51579773],
           [40.28003303, 37.18956223],
           [50.09555187, 35.31928964],
           [42.81456172, 23.13071769],
           [ 7.48942893, 88.382881  ],
           [20.39775419, 60.95227849],
           [22.72369685, 49.69477172],
           [ 9.7363189 , 74.9417672 ]])



Because this is a fuzzy clustering algorithm, each observations doesn't
belong to just one class. Each observations has a *membership* to each
class (i.e.: the probability of belonging to each class). Let's see how
that looks for the first observation:

.. code:: python3

    k9.U[0]




.. parsed-literal::
    \
    array([1.33489186e-03, 2.27479762e-05, 2.87618787e-02, 9.69688426e-01,
           1.69133208e-05, 1.59578403e-06, 3.90639066e-05, 7.21471497e-06,
           3.26975170e-06, 1.23998321e-04])



We can see that the highest membership corresponds to the 4th class.
Also, you should notice that there are 10 memberships. The last value
corresponds to the membership of the *extragrade* class, which defines
if the observations is an outlier or not.

The ``k9`` instance has the ``hard_clusters`` attribute, so we can
identify each observation. This is just the maximum membership, so use
this hard clusters with caution because in some cases the membership of
2 classes can be very similar (e.g.: [0, 0.49, 0.51]).

.. code:: python3

    k9.hard_clusters[0]




.. parsed-literal::
    \
    '4'



Because we used 2 predictors, we can use the
:func:`~.utils.extragrade_limit` function to visualise the data space:

.. code:: python3

    from pyPTF.utils import extragrade_limit

    extragrade_limit(model1500, k9)



.. image:: _static/example_files/example_29_1.png


The final step is to "merge" our PTF and FKMEx instances. To do that we
can use the ``PTF.add_uncertainty`` method.

.. code:: python3

    model1500.add_uncertainty(k9)

Now that we have an uncertainty associated with our PTF, let's make a
prediction again, using the ``new_data`` that we defined before:

.. code:: python3

    model1500.predict(new_data)




.. parsed-literal::
    \
    array([[0.1621723 , 0.258557  , 0.33608876],
           [0.18210334, 0.27919531, 0.35693219],
           [0.17187183, 0.2689638 , 0.34670068],
           [0.21510369, 0.28697602, 0.35618361],
           [0.18790757, 0.2793628 , 0.35554915]])



As you can see, now we predict the same value than before (middle
column) but accompanied with the prediction interval limits.

Sharing and publishing your PTF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Your new ``PTF`` can be shared in a few different ways. The first one is
to save an exact copy of the model (using the ``pickle`` library),
including the training data:

.. code:: python3

    model1500.save('path_to_file.ptf', 'full')

The second options is to save just the parameters needed to reconstruct
the ``PTF``:

.. code:: python3

    model1500.save('path_to_file.json', 'minimal')

which creates the ``json`` file:

.. code:: python3

    import json
    import pprint
    pp = pprint.PrettyPrinter(indent=2)
    with open('path_to_file.json', 'r') as f:
        from_json = json.load(f)
    pp.pprint(from_json)


.. parsed-literal::
    \
    { 'ptf': '0.738*clay/(2*clay + sand + 0.322)',
      'uncertainty': { 'PIC': [ [-0.10399041233424072, 0.09101175510118556],
                                [-0.052261279014767945, 0.09768409992575605],
                                [-0.0717422932372485, 0.06908565671025887],
                                [-0.09709196629264981, 0.07773687907924977],
                                [-0.07899126891260226, 0.07125985328620485],
                                [-0.03145764013905392, 0.10200211968771555],
                                [-0.06558316869521735, 0.1415813866833374],
                                [-0.050589491047846255, 0.07972005251321522],
                                [-0.03535014831852784, 0.05164985168147217],
                                [-0.22479607051820627, 0.2646349845652226]],
                       'W': [ [0.01779550120786616, 0.012128237610843124],
                              [0.012128237610843123, 0.010459714555939784]],
                       'centroids': [ [28.33741153799354, 59.08412784894298],
                                      [16.88978997140965, 71.51579773498064],
                                      [40.2800330337506, 37.18956222524247],
                                      [50.09555186537584, 35.31928964425378],
                                      [42.8145617201694, 23.130717688915414],
                                      [7.489428930183593, 88.38288100350492],
                                      [20.397754192987453, 60.95227849198744],
                                      [22.723696852026723, 49.694771718947145],
                                      [9.736318895288832, 74.94176720217213]]}}

If you want to publish the PTF, you have to provide the same information.
You can use the :func:`~.utils.summary` function to generate a latex table:

.. code:: python3

    from pyPTF.utils import summary

    print(summary(model1500))


.. parsed-literal::
    \
    \\begin{tabular}{ccccc}
    \\toprule
    {} & clay & sand & PI_{L} & PI_{U} \\\\
    \\midrule
    Clusters & \\multicolumn{2}{c}{Centroids} & \\multicolumn{2}{c}{Cluster residuals} \\\\
    \\cmidrule(rl){2-3} \\cmidrule(rl){4-5}
    1 & 28.34 & 59.08 & -0.10 & 0.09 \\\\
    2 & 16.89 & 71.52 & -0.05 & 0.10 \\\\
    3 & 40.28 & 37.19 & -0.07 & 0.07 \\\\
    4 & 50.10 & 35.32 & -0.10 & 0.08 \\\\
    5 & 42.81 & 23.13 & -0.08 & 0.07 \\\\
    6 & 7.49 & 88.38 & -0.03 & 0.10 \\\\
    7 & 20.40 & 60.95 & -0.07 & 0.14 \\\\
    8 & 22.72 & 49.69 & -0.05 & 0.08 \\\\
    9 & 9.74 & 74.94 & -0.04 & 0.05 \\\\
    Eg & -- & -- & -0.22 & 0.26 \\\\
    \\bottomrule
    \\end{tabular}

Which renders to something like this:

.. image:: _static/example_files/table.png
